import numpy as np
import pandas as pd


def lowercase_text(text):
    return text.lower()


def word_count(text):
    try:
        if text == 'No description yet':
            return 0

        text = lowercase_text(text)
        return len([w for w in text.split(' ')])
    except:
        return 0


def split_category(text) -> tuple[str, str, str]:
    try:
        return text.split('/')
    except:
        return "No label", "No label", "No label"


def brand_finder(line, brands):
    brand = line.iloc[0]
    name = line.iloc[1]
    name_split = name.split(' ')

    if brand == 'missing':
        for x in name_split:
            if x in brands:
                return name
    if name in brands:
        return name

    return brand


def clean_data(raw_path):
    train_df = pd.read_table(raw_path)
    train_df['desc_len'] = train_df['item_description'].apply(lambda x: word_count(x))
    train_df['name_len'] = train_df['name'].apply(lambda x: word_count(x))
    train_df['item_condition_id'] = train_df['item_condition_id'].astype('str')
    train_brands = set(train_df['brand_name'].values)
    train_df['brand_name'] = train_df[['brand_name', 'name']].apply(brand_finder, axis=1, brands=train_brands)
    train_df['subcategory_1'], train_df['subcategory_2'], train_df['subcategory_3'] = zip(
        *train_df['category_name'].apply(lambda x: split_category(x)))

    train_df.fillna({"category_name": "missing"}, inplace=True)
    train_df.fillna({"brand_name": "missing"}, inplace=True)
    train_df.fillna({"item_description": "missing"}, inplace=True)
    train_df.fillna({"category_name": "missing"}, inplace=True)
    # train_df['item_description'].replace('No description yet', "missing", inplace=True)

    train_df["subcategory_1"] = train_df["subcategory_1"].apply(lowercase_text)
    train_df["subcategory_2"] = train_df["subcategory_2"].apply(lowercase_text)
    train_df["subcategory_3"] = train_df["subcategory_3"].apply(lowercase_text)
    train_df["brand_name"] = train_df["brand_name"].apply(lowercase_text)
    train_df["item_description"] = train_df["item_description"].apply(lowercase_text)
    train_df["name"] = train_df["name"].apply(lowercase_text)
    train_df["is_brand_missing"] = np.where(train_df["brand_name"] == "missing", 1, 0)
    train_df["is_item_description_missing"] = np.where(train_df["item_description"] == "missing", 1, 0)

    return train_df

