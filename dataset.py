import torch


class ProductRatingDataset(object):
    def __init__(self, edge_index, edge_values):
        self.edge_index = edge_index
        self.edge_values = edge_values

    def __len__(self):
        return len(self.edge_index)

    def __getitem__(self, item):
        return {
            "edge_index": torch.tensor(self.edge_index, dtype=torch.long),
            "edge_values": torch.tensor(self.edge_values, dtype=torch.long),
        }
