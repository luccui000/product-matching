import pandas as pd
import torch
from sklearn import model_selection
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import DataLoader
import pytorch_lightning as pl

import config
import utils
from model import ProductRatingGCNModel

# Xử lý dữ liệu
# - Thay thế những trường hợp data bị null thành missing
# - Convert dữ liệu category thành 3 cột sub category
# - Thêm cột để đánh dấu dữ liệu bị missing
df = pd.read_csv('data/raw/test.csv', names=['userId', 'productId', 'rating', 'timestamp'])

"""
    parquet là định dạng lưu trữ các cột trong hệ sinh thái của Hadoop 
    theo mô hình của bài blog https://onetech.vn/blog/ung-dung-ai-matching-ket-noi-nguoi-mua-va-nguoi-ban-18455
    dữ liệu được thu thập từ hệ thống sau đó sẽ chuyển thành file parquet
    Ngoài ra có thể tận dụng được AWS Glue để chuyển file csv thành parquet, chi tiết hướng dẫn xem ở trong bài blog
    https://onetech.vn/blog/aws-glue-chia-khoa-cho-viec-phan-tich-du-lieu-hieu-qua-17973
"""
# df.to_parquet("./data/processed/train_processed.parquet")


"""
    Tiến hành đọc file parquet
"""
# train = pd.read_parquet("./data/processed/train_processed.parquet")
# test = pd.read_parquet("./data/processed/test_processed.parquet")

# print(f"Train len: {len(train)}")
# print(f"Test len: {len(test)}")


lb_users = LabelEncoder()
lb_products = LabelEncoder()

df.userId = lb_users.fit_transform(df.userId.values)
df.productId = lb_products.fit_transform(df.productId.values)

edge_index, edge_values = utils.load_edge_csv(
    df,
    src_index_col='userId',
    dst_index_col='productId',
    link_index_col='rating',
)

edge_index = torch.LongTensor(edge_index)
edge_values = torch.tensor(edge_values)


num_users = len(df['userId'].unique())
num_products = len(df['productId'].unique())

print(f"num_users {num_users}, num_products {num_products}")

num_interactions = edge_index.shape[1]
all_indices = [i for i in range(num_interactions)]

train_indicates, valid_indicates = model_selection.train_test_split(all_indices, test_size=0.20, random_state=42)

train_edge_index = edge_index[:, train_indicates]
train_edge_value = edge_values[train_indicates]

val_edge_index = edge_index[:, valid_indicates]
val_edge_value = edge_values[valid_indicates]

train_edge_index, train_edge_values = utils.convert_r_mat_edge_index_to_adj_mat_edge_index(
    train_edge_index,
    train_edge_value,
    num_users=num_users,
    num_products=num_products
)
val_edge_index, val_edge_values = utils.convert_r_mat_edge_index_to_adj_mat_edge_index(
    val_edge_index,
    val_edge_value,
    num_users=num_users,
    num_products=num_products
)

train_dataset = utils.build_dataset(train_edge_index, train_edge_values)
val_dataset = utils.build_dataset(val_edge_index, val_edge_values)

train_loader = DataLoader(
    dataset=train_dataset,
    batch_size=config.BATCH_SIZE,
    shuffle=True,
    num_workers=config.NUM_WORKERS
)

val_loader = DataLoader(
    dataset=val_dataset,
    batch_size=config.BATCH_SIZE,
    shuffle=True,
    num_workers=config.NUM_WORKERS
)


model = ProductRatingGCNModel(num_users=num_users, num_products=num_products)
trainer = pl.Trainer(max_epochs=config.EPOCHS)
trainer.fit(model, train_dataloaders=train_loader, val_dataloaders=val_loader)
# model.save_hyperparameters('test.h5')

