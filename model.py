import utils
import torch
import torch.nn as nn
import torch.optim as optim
import pytorch_lightning as pl
from torch_geometric.nn.conv.gcn_conv import gcn_norm


class ProductRatingGCNModel(pl.LightningModule):
    def __init__(self, num_users, num_products, embedding_dim=64, K=10):
        super().__init__()
        self.K = K
        self.num_users = num_users
        self.num_products = num_products
        self.embedding_dim = embedding_dim

        self.lr = 0.2
        self.optimizer = torch.optim.Adam
        self.add_self_loops = False
        self.loss_fn = nn.MSELoss()

        '''
        Thực hiện embedding vector
        '''
        self.user_embeds = nn.Embedding(self.num_users, embedding_dim=self.embedding_dim)
        self.product_embeds = nn.Embedding(self.num_products, embedding_dim=self.embedding_dim)

        '''
            Khởi tạo trọng số của tensor theo phân phối chuẩn
            Việc khởi tạo trọng số theo phân phối chuẩn có thể 
            giúp mô hình học sâu khởi đầu từ một trạng thái tốt 
            hơn và giúp nó hội tụ nhanh hơn trong quá trình huấn luyện
        '''
        nn.init.normal_(self.user_embeds.weight, std=0.1)
        nn.init.normal_(self.product_embeds.weight, std=0.1)

        '''
        Linear layer mục đích dùng để dự đoán giá trị tiếp theo một cách tuyến tính
        '''
        self.linear_layers = nn.Linear(self.embedding_dim + self.embedding_dim, 1)

    def forward(self, edge_index, edge_values):
        """
        Tính toán bậc của các đỉnh trong đồ thị.
        Kết quả:
            - Phần đầu tiên là ma trận biểu diễn cạnh sau khi đã được chuẩn hóa.
            - Phần thứ hai là các hệ số chuẩn hóa tương ứng với mỗi cạnh trong đồ thị.
        """
        edge_index_norm = gcn_norm(edge_index=edge_index,
                                   add_self_loops=self.add_self_loops)

        emb_0 = torch.cat([self.user_embeds.weight, self.product_embeds.weight])
        embeds = [emb_0]
        emb_k = emb_0

        for i in range(self.K):
            emb_k = self.propagate(edge_index=edge_index_norm[0], x=emb_k, norm=edge_index_norm[1])
            embeds.append(emb_k)

        """
        Chồng các embedding lại thành một tensor: 
            Hàm torch.stack(embeds, dim=1) chồng các embedding của các đỉnh ở mỗi lớp (trong danh sách embeds) 
            lại thành một tensor mới. Điều này giúp chúng ta tổ chức các embedding của mỗi đỉnh theo từng lớp.
        Tính embedding cuối cùng:
            Sau khi chồng các embedding lại, chúng ta tính embedding cuối cùng cho mỗi đỉnh bằng cách lấy 
            trung bình cộng của các embedding ở mỗi lớp. Điều này được thực hiện thông qua hàm torch.mean(embeds, dim=1),
            trong đó dim=1 chỉ định rằng chúng ta muốn tính trung bình cộng theo chiều thứ hai của tensor, tức là theo từng lớp.
        Tách embedding cho người dùng và sản phẩm: 
            Sau khi tính được embedding cuối cùng cho tất cả các đỉnh, chúng ta cần tách chúng ra 
            thành embedding cho người dùng và embedding cho sản phẩm. 
            Điều này được thực hiện thông qua hàm torch.split(emb_final, [self.num_users, self.num_products]). Trong đó:
        emb_final là tensor chứa embedding cuối cùng cho tất cả các đỉnh.
            [self.num_users, self.num_products] là danh sách chứa số lượng người dùng và số lượng sản phẩm. 
            Hàm torch.split sẽ tách tensor emb_final thành hai tensor con, 
            mỗi tensor chứa embedding cho người dùng và sản phẩm tương ứng.
        """
        embeds = torch.stack(embeds, dim=1)
        emb_final = torch.mean(embeds, dim=1)
        users_emb_final, products_emb_final = torch.split(emb_final, [self.num_users, self.num_products])
        r_mat_edge_index, _ = utils.convert_adj_mat_edge_index_to_r_mat_edge_index(
            edge_index,
            edge_values,
            self.num_users,
            self.num_products
        )
        src, dest = r_mat_edge_index[0], r_mat_edge_index[1]

        user_embeds = users_emb_final[src]
        product_embeds = products_emb_final[dest]

        output = torch.cat([user_embeds, product_embeds], dim=1)

        return self.linear_layers(output)

    def training_step(self, batch, batch_idx):
        edge_index, edge_values, loss = self._common_step(batch, batch_idx)
        self.log('train_loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        edge_index, edge_values, loss = self._common_step(batch, batch_idx)
        self.log('val_loss', loss)
        return loss

    def _common_step(self, batch, batch_idx):
        edge_index, edge_values, target = batch
        pred_ratings = self.forward(edge_index, edge_values)
        loss = self.loss_function(pred_ratings, target)

        return edge_index, edge_values, loss

    def configure_optimizers(self):
        return optim.Adam(self.parameters(), lr=self.lr)
