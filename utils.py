import pandas as pd
import torch
from dataset import ProductRatingDataset
from torch_sparse import SparseTensor


def load_edge_csv(df: pd.DataFrame, src_index_col, dst_index_col, link_index_col, rating_threshold=3.5):
    src = [userId for userId in df[src_index_col]]
    dst = [productId for productId in df[dst_index_col]]
    link_vals = df[link_index_col].values
    edge_attr = torch.from_numpy(df[link_index_col].values).view(-1, 1).to(torch.long) >= rating_threshold

    edge_values = []

    edge_index = [[], []]
    for i in range(edge_attr.shape[0]):
        if edge_attr[i]:
            edge_index[0].append(src[i])
            edge_index[1].append(dst[i])
            edge_values.append(link_vals[i])

    return edge_index, edge_values


"""
Hàm chuyển ma trận kề thành qua trận tương quan
0 1 1 0
1 0 1 1
1 1 0 1
0 1 1 0

-> 

(1, 2)
(1, 3)
(2, 1)
(2, 3)
(2, 4)
(3, 1)
(3, 2)
(3, 4)
(4, 2)
(4, 3)

"""


def convert_adj_mat_edge_index_to_r_mat_edge_index(input_edge_index, input_edge_values, num_users, num_products):
    sparse_input_edge_index = SparseTensor(row=input_edge_index[0],
                                           col=input_edge_index[1],
                                           value=input_edge_values,
                                           sparse_sizes=((num_users + num_products), num_users + num_products))

    adj_mat = sparse_input_edge_index.to_dense()
    interact_mat = adj_mat[: num_users, num_users:]

    r_mat_edge_index = interact_mat.to_sparse_coo().indices()
    r_mat_edge_values = interact_mat.to_sparse_coo().values()

    return r_mat_edge_index, r_mat_edge_values


def convert_r_mat_edge_index_to_adj_mat_edge_index(input_edge_index, input_edge_values, num_users, num_products):
    R = torch.zeros((num_users, num_products))

    for i in range(len(input_edge_index[0])):
        row_idx = input_edge_index[0][i]
        col_idx = input_edge_index[1][i]
        R[row_idx][col_idx] = input_edge_values[i]

    R_transpose = torch.transpose(R, 0, 1)

    adj_mat = torch.zeros((num_users + num_products, num_users + num_products))
    adj_mat[: num_users, num_users:] = R.clone()
    adj_mat[num_users:, : num_users] = R_transpose.clone()

    adj_mat_coo = adj_mat.to_sparse_coo()
    adj_mat_coo_indices = adj_mat_coo.indices()
    adj_mat_coo_values = adj_mat_coo.values()
    return adj_mat_coo_indices, adj_mat_coo_values


def build_dataset(edge_index, edge_values):
    return ProductRatingDataset(
        edge_index,
        edge_values,
    )
